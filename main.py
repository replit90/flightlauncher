from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")


templates = Jinja2Templates(directory="templates")

FLIGHTS = [
    (1, '/fl-12', 'finished'),
    (2, '/fl22', 'analysing'),
    (3, '/met', 'finished'),
    (4, '/fasf', 'analysis'),
]

@app.get("/flights", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse("item.html", {"request": request, "id": 11, "flights": FLIGHTS})
